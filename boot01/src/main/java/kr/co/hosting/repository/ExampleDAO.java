package kr.co.hosting.repository;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

//import org.apache.ibatis.annotations.Mapper; 
//import org.apache.ibatis.annotations.Select; 

//@Mapper 
//public interface ExampleDAO { 
//	@Select("SELECT NOW()") 
//	String getCurrentDateTime(); 
//}


@Repository
public class ExampleDAO {
	
	@Autowired
	@Qualifier("firstSqlSessionTemplate")
	private SqlSession sqlSession;
	
	public String getCurrentDateTime() {
		return sqlSession.selectOne("kr.co.hosting.mapper.exampleMapper.getCurrentDateTime");

	}

}

