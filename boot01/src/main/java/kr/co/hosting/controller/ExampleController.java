package kr.co.hosting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.co.hosting.service.ExampleService;


@RestController
public class ExampleController {
	
	@Autowired
	private ExampleService exampleService;
	
	
	@RequestMapping("/")
	public String index(Model model) {
		
		return exampleService.getCurrentDataTime();
	}
	

}
