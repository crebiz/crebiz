package kr.co.hosting.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.hosting.repository.ExampleDAO;

@Service
public class ExampleService {
	
	 @Autowired
	 private ExampleDAO exampleDAO;
	 
	 public String getCurrentDataTime() {
		 return exampleDAO.getCurrentDateTime();

	 }


}
